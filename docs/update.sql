alter table config.person
	add updated_by integer not null;

alter table config.person
	add created_by integer not null;

alter table config.person
add constraint person_pk_2
    unique (mail);

alter table main.shipments
	add updated_by integer not null;

alter table main.shipments
	add created_by integer not null;

alter table main.shipments
    add delivered bool default FALSE not null;

alter table main.shipments
    add comments text;

create table config.store
(
    id serial not null,
    name varchar(30) not null,
    address varchar(50),
    phone varchar(20),
    country varchar(10),
    type char,
    active BOOLEAN
);

comment on column config.store.type is 'O= Origin,
D= Destination';

create unique index store_id_uindex
	on config.store (id);

alter table config.store
    add constraint store_pk
        primary key (id);

--ROLES
create table config.role
(
    id serial not null,
    name varchar(20) not null
);

create unique index role_id_uindex
	on config.role (id);

alter table config.role
    add constraint role_pk
        primary key (id);

alter table config.person
    add role_id int not null;

alter table config.person
    add store_id int not null;

alter table config.person
    add constraint person_role_fk
        foreign key (role_id) references config.role
            on delete restrict;

alter table config.person
    add constraint person_store_fk
        foreign key (store_id) references config.store
            on delete restrict;
alter table main.shipments alter column origin_location type INT using origin_location::INT;

alter table main.shipments alter column destination_location type INT using destination_location::INT;

alter table main.shipments
    add constraint shipments_destination_fk
        foreign key (destination_location) references config.store;

alter table main.shipments
    add constraint shipments_origin_fk
        foreign key (origin_location) references config.store;

alter table config.store rename column type to folio;

comment on column config.store.folio is null;

alter table config.store alter column folio type varchar(5) using folio::varchar(5);

alter table main.shipments alter column sender_phone type varchar(30) using sender_phone::varchar(30);

alter table main.shipments alter column receiver_phone type varchar(30) using receiver_phone::varchar(30);

alter table main.shipments
    add shipmentreport_id int;

alter table main.shipments
    add constraint shipments_report_fk
        foreign key (shipmentreport_id) references main.shipments_report;

alter table main.shipments
    add quantity int;

alter table main.shipments alter column cost drop not null;

drop index main.shipments_shipment_number_uindex;--temporal

alter table config.store
    add folio_counter INTEGER default 0 not null;

alter table main.shipments alter column shipment_number type INTEGER using shipment_number::INTEGER;
