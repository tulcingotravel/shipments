CREATE DATABASE tulcingo_travel;

CREATE SCHEMA config;
CREATE SCHEMA main;

--PERSON--
create table config.person
(
	id serial not null,
	name varchar(50) not null,
	firstname varchar(50) not null,
	lastname varchar(50),
	birthdate date,
	created_at timestamp not null,
	updated_at timestamp not null,
	active boolean,
	phone varchar(20),
	mail varchar(100),
	password varchar(100) not null
);

create unique index person_id_uindex
	on config.person (id);

alter table config.person
	add constraint person_pk
		primary key (id);

--SHIPMENTS--
create table main.shipments
(
	id serial not null,
	shipment_number varchar(10) not null,
	origin_location varchar(50) not null,
	destination_location varchar(50) not null,
	sender_name varchar(100) not null,
	sender_phone varchar(90),
	sender_address varchar(100),
	receiver_name varchar(100) not null,
	receiver_phone varchar(90),
	receiver_address varchar(100),
	shipment_content text not null,
	sub varchar(10),
	weight varchar(10),
	cost varchar(20) not null,
	v_estimated varchar(10),
	tax varchar(10)
);

create unique index shipments_id_uindex
	on main.shipments (id);

create unique index shipments_shipment_number_uindex
	on main.shipments (shipment_number);

alter table main.shipments
	add constraint shipments_pk
		primary key (id);

alter table main.shipments
	add created_at timestamp not null;

alter table main.shipments
	add updated_at timestamp not null;

--SHIPMENTS REPORT
create table main.shipments_report
(
    id serial not null,
    created_at timestamp not null,
    created_by int not null,
    start_date timestamp not null,
    end_date timestamp not null
);

create unique index shipments_report_id_uindex
    on main.shipments_report (id);

alter table main.shipments_report
    add constraint shipments_report_pk
        primary key (id);

--FOLIOS
create table config.folios
(
    id serial not null,
    store_id int not null,
    counter int not null
);

create unique index folios_id_uindex
    on config.folios (id);

create unique index folios_store_id_uindex
    on config.folios (store_id);

alter table config.folios
    add constraint folios_pk
        primary key (id);

alter table config.folios
    add constraint folios_store_fk
        foreign key (store_id) references config.store;