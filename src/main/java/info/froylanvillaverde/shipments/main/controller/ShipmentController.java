package info.froylanvillaverde.shipments.main.controller;

import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.model.ShipmentDto;
import info.froylanvillaverde.shipments.main.service.ShipmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/main/shipment")
@CrossOrigin(origins = "http://localhost:3000")
public class ShipmentController {

    @Autowired
    private ShipmentService shipmentService;

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Shipment>> findAllShipments(){
        LOG.info("start findAllShipments.");
        return ResponseEntity.ok(shipmentService.findAllShipments());
    }

    @GetMapping("/{shipmentId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Shipment> getShipment(@PathVariable Long shipmentId){
        LOG.info("getShipment() start - with id: {}", shipmentId);
        Shipment shipmentReturned = shipmentService.getShipmentByUserStores(shipmentId);
        LOG.info("getShipment() end - object returned : {}", shipmentReturned);
        return ResponseEntity.ok(shipmentReturned);
    }

    @PutMapping("/{shipmentId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Shipment> updateShipment(@RequestBody ShipmentDto shipment){
        LOG.info("starting updateShipment - with object request: {}", shipment);
        Shipment shipmentUpdated = shipmentService.updateShipment(shipment);
        LOG.info("finishing updateShipment");
        return new ResponseEntity<Shipment>(shipmentUpdated, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity saveShipment(@RequestBody ShipmentDto shipmentDto){
        LOG.info("starting saveShipment - with object request: {}", shipmentDto);
        shipmentService.saveShipmentDto(shipmentDto);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(shipmentDto.getId())
                .toUri();
        LOG.info("finishing saveShipment");
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{shipmentId}")
    public ResponseEntity<Void> deleteById(@PathVariable long shipmentId){
        shipmentService.deleteById(shipmentId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/search")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Shipment>> getShipmentsFiltered(@RequestParam String filter){
        LOG.info("getShipmentsFiltered() starting - with filter: {}", filter);
        List<Shipment> shipmentsReturned = shipmentService.getShipmentShipmentsFiltered(filter);
        LOG.info("getShipmentsFiltered() finishing");
        return ResponseEntity.ok(shipmentsReturned);
    }
}
