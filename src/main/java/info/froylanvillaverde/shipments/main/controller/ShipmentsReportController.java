package info.froylanvillaverde.shipments.main.controller;

import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import info.froylanvillaverde.shipments.main.model.ShipmentDto;
import info.froylanvillaverde.shipments.main.model.mapper.ShipmentMapper;
import info.froylanvillaverde.shipments.main.service.ShipmentService;
import info.froylanvillaverde.shipments.main.service.ShipmentsReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/main/report/shipment")
@CrossOrigin(origins = "http://localhost:3000")
public class ShipmentsReportController {

    @Autowired
    private ShipmentsReportService shipmentsReportService;
    @Autowired
    private ShipmentService shipmentService;
    @Autowired
    private ShipmentMapper shipmentMapper;

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<ShipmentsReport>> findAllShipmentsReport() {

        return ResponseEntity.ok(shipmentsReportService.findAllShipmentsReport());
    }

    @GetMapping("/by-user")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<ShipmentsReport>> findAllShipmentsReportByUser() {

        return ResponseEntity.ok(shipmentsReportService.findAllShipmentsReportByUser());
    }

    @GetMapping("/details")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Shipment>> findAllShipmentsByUser() {

        return ResponseEntity.ok(shipmentsReportService.getshipmentsReportsByUser());
    }

    @GetMapping("/{shipmentReportId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<ShipmentsReport> findById(@PathVariable Long shipmentReportId) {

        return ResponseEntity.ok(shipmentsReportService.getshipmentsReportsById(shipmentReportId));
    }

    @GetMapping("/{shipmentReportId}/detail")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Shipment>> findShipmentsByShipmentsReportsId(@PathVariable Long shipmentReportId) {

        return ResponseEntity.ok(shipmentsReportService.getShipmentsByShipmentsReportsId(shipmentReportId));
    }

    ///Revisar implementación
    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity saveShipmentsReport() {//@RequestBody Map<String, String> storeUserIds){
        LOG.info("starting saveShipmentsReport ");
        Person principal = shipmentsReportService.getPersonByPrincipal();
        Long shipmentsReportId = 0L;
        Long storeId = principal.getStore().getId();//storeUserIds.get("storeId");
        Long userId = principal.getId();//storeUserIds.get("userId");

        List<Shipment> shipments = shipmentService.getOpenShipmentsByUserAndStore(userId, storeId);
        if (!shipments.isEmpty()) {
            ShipmentsReport shipmentsReport = shipmentsReportService.createShipmentService();

            shipments.stream().forEach((shipment -> {
                ShipmentDto shipmentDto = shipmentMapper.convertToDto(shipment);
                shipmentDto.setShipmentReportId(shipmentsReport.getId());
                shipmentService.updateShipment(shipmentDto);
            }));
            shipmentsReportId = shipmentsReport.getId();
        }
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(shipmentsReportId)
                .toUri();

        LOG.info("finishing saveShipmentsReport");
        return ResponseEntity.created(uri).build();
    }
}
