package info.froylanvillaverde.shipments.main.service;

import info.froylanvillaverde.shipments.config.dao.PersonRepository;
import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.config.service.StoreService;
import info.froylanvillaverde.shipments.main.dao.ShipmentRepository;
import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import info.froylanvillaverde.shipments.main.model.ShipmentDto;
import info.froylanvillaverde.shipments.main.model.mapper.ShipmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShipmentService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ShipmentRepository shipmentRepository;
    @Autowired
    private ShipmentMapper shipmentMapper;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private StoreService storeService;

    public List<Shipment> findAllShipments() {
        LOG.debug("findAllShipments()");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        LOG.info("user principal: {}", currentPrincipalName);
        Person person = personRepository.findPersonByMail(currentPrincipalName);

        List<Shipment> shipments = shipmentRepository.findAllByOriginLocationOrDestinationLocation(person.getStore(), person.getStore());
        LOG.debug("Shipments end");
        return shipments;
    }

    public Shipment getShipment(Long id) {
        LOG.debug("getShipment() with id [{}]", id);
        Shipment shipmentReturned;

        if (id == -1) {
            Store store = new Store();
            ShipmentsReport shipmentsReport = new ShipmentsReport();
            shipmentReturned = new Shipment();
            shipmentReturned.setOriginLocation(store);
            shipmentReturned.setDestinationLocation(store);
            shipmentReturned.setShipmentsReport(shipmentsReport);
        } else {
            shipmentReturned = shipmentRepository.findById(id).get();
        }
        LOG.debug("getShipment() end - shipment returned: {}", shipmentReturned);
        return shipmentReturned;
    }

    public Shipment getShipmentByUserStores(Long id) {
        LOG.debug("getShipment() with id [{}]", id);
        Shipment shipmentReturned;

        if (id == -1) {
            Store store = new Store();
            ShipmentsReport shipmentsReport = new ShipmentsReport();
            shipmentReturned = new Shipment();
            shipmentReturned.setOriginLocation(store);
            shipmentReturned.setDestinationLocation(store);
            shipmentReturned.setShipmentsReport(shipmentsReport);
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();
            LOG.info("user principal: {}", currentPrincipalName);
            Person person = personRepository.findPersonByMail(currentPrincipalName);

            shipmentReturned = shipmentRepository.findByIdAndOriginLocationOrDestinationLocation(id, person.getStore(), person.getStore());
        }
        LOG.debug("getShipment() end - shipment returned: {}", shipmentReturned);
        return shipmentReturned;
    }

    public void saveShipmentDto(ShipmentDto shipmentDto) {
        LOG.debug("Shipment saved {}", shipmentDto.toString());
        Shipment shipment = shipmentMapper.convertToEntity(shipmentDto);
        saveShipment(shipment);

    }

    public void saveShipment(Shipment shipment) {
        LOG.debug("Shipment saved {}", shipment.toString());
        Person person = getPersonByPrincipal();
        Store store = storeService.getStore(person.getStore().getId());
        store.setFolioCounter(shipment.getShipmentNumber() + 1L);
        shipmentRepository.save(shipment);
        storeService.updateStore(store);
    }

    public Shipment updateShipment(ShipmentDto shipmentDto) {
        Shipment shipment = shipmentMapper.convertToEntity(shipmentDto);
        shipment.setUpdatedAt(LocalDateTime.now());
        LOG.debug("Shipment updated {}", shipment.toString());
        return shipmentRepository.save(shipment);
    }

    public void deleteById(Long id) {
        LOG.debug("Shipment deleted {}", id);
        shipmentRepository.deleteById(id);
    }

    public List<Shipment> getOpenShipmentsByUserAndStore(Long userId, Long storeId) {
        LOG.info("starting getOpenShipmentsByUserAndStore with userId: {} and storeId: {}", userId, storeId);
        Store store = storeService.getStore(storeId);
        List<Shipment> shipments = shipmentRepository.findAllByCreatedByAndOriginLocationAndAndDelivered(userId, store, false);
        LOG.info("finishing getOpenShipmentsByUserAndStore");
        return shipments;
    }

    private Person getPersonByPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        LOG.info("user principal: {}", currentPrincipalName);
        return personRepository.findPersonByMail(currentPrincipalName);
    }

    public List<Shipment> getShipmentShipmentsFiltered(String filter) {
        LOG.debug("starting getShipmentShipmentsFiltered()");
        List<Shipment> shipments = findAllShipments();
        List<Shipment> shipmentsFiltered = shipments.stream().filter(shipment -> {
            return shipment.getSenderPerson().toLowerCase().contains(filter.toLowerCase())
                    || shipment.getReceiverPerson().toLowerCase().contains(filter.toLowerCase());
        })
                .collect(Collectors.toList());
        LOG.debug("finishing getShipmentShipmentsFiltered()");
        return shipmentsFiltered;
    }
}
