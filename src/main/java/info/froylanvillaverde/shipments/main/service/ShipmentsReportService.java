package info.froylanvillaverde.shipments.main.service;

import info.froylanvillaverde.shipments.config.dao.PersonRepository;
import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.main.dao.ShipmentRepository;
import info.froylanvillaverde.shipments.main.dao.ShipmentsReportRepository;
import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import info.froylanvillaverde.shipments.main.model.ShipmentDto;
import info.froylanvillaverde.shipments.main.model.mapper.ShipmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShipmentsReportService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ShipmentRepository shipmentRepository;
    @Autowired
    private ShipmentMapper shipmentMapper;
    @Autowired
    private ShipmentsReportRepository shipmentsReportRepository;
    @Autowired
    private PersonRepository personRepository;

    public List<ShipmentsReport> findAllShipmentsReport(){
        LOG.debug("findAllShipmentsReport() start");

        List<ShipmentsReport> shipmentsReports = shipmentsReportRepository.findAll();
        LOG.debug("findAllShipmentsReport() end");
        return shipmentsReports;
    }

    public List<ShipmentsReport> findAllShipmentsReportByUser(){
        LOG.debug("findAllShipmentsReport() start");

        Person person = getPersonByPrincipal();
        List<ShipmentsReport> shipmentsReports = shipmentsReportRepository.findAllByCreatedBy(person.getId());
        LOG.debug("findAllShipmentsReport() end");
        return shipmentsReports;
    }

    public List<Shipment> getshipmentsReportsByUser(){
        LOG.debug("getshipmentsReports() start");
        List<Shipment> shipmentsReturned;

        Person person = getPersonByPrincipal();
        shipmentsReturned = shipmentRepository.findAllByCreatedBy(person.getId());
        LOG.debug("getshipmentsReports() end - shipmentReport returned: {}, with ",shipmentsReturned);
        return shipmentsReturned;
    }

    public ShipmentsReport getshipmentsReportsById(Long id){
        LOG.debug("getshipmentsReportsById() start");
        ShipmentsReport shipmentsReturned;

        shipmentsReturned = shipmentsReportRepository.findById(id).get();
        LOG.debug("getshipmentsReportsById() end - shipmentReport returned: {} ",shipmentsReturned);
        return shipmentsReturned;
    }

    public List<Shipment> getShipmentsByShipmentsReportsId(Long id){
        LOG.info("getShipmentsByShipmentsReportsId() start");
        List<Shipment> shipmentsReturned;
        ShipmentsReport shipmentsReport = shipmentsReportRepository.findById(id).get();

        shipmentsReturned = shipmentRepository.findAllByShipmentsReport(shipmentsReport);
        LOG.info("getShipmentsByShipmentsReportsId() end - shipments returned: {}, with ",shipmentsReturned);
        return shipmentsReturned;
    }

    public Person getPersonByPrincipal(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        LOG.info("user principal: {}", currentPrincipalName);
        return personRepository.findPersonByMail(currentPrincipalName);
    }

    public ShipmentsReport createShipmentService(){
        Person person = getPersonByPrincipal();
        ShipmentsReport shipmentsReport = new ShipmentsReport();
        shipmentsReport.setCreatedBy(person);
        shipmentsReport.setCreatedAt(LocalDateTime.now());
        shipmentsReport.setStartDate(LocalDateTime.now());
        shipmentsReport.setEndDate(LocalDateTime.now());
        return shipmentsReportRepository.save(shipmentsReport);
    }
}
