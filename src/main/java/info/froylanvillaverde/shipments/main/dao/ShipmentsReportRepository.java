package info.froylanvillaverde.shipments.main.dao;

import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipmentsReportRepository extends JpaRepository<ShipmentsReport, Long> {

    public List<ShipmentsReport> findAllByCreatedBy(Long createdBy);
}
