package info.froylanvillaverde.shipments.main.dao;

import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {

    public List<Shipment> findAllByOriginLocationOrDestinationLocation(Store origin, Store destination);

    public Shipment findByIdAndOriginLocationOrDestinationLocation(Long id, Store origin, Store destination);

    public List<Shipment> findAllByCreatedBy(Long person);

    public List<Shipment> findAllByShipmentsReport(ShipmentsReport shipmentsReport);

    public List<Shipment> findAllByCreatedByAndOriginLocationAndAndDelivered(Long createdBy, Store originLocation, Boolean delivered);

}
