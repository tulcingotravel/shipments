package info.froylanvillaverde.shipments.main.dao.entity;

import info.froylanvillaverde.shipments.config.dao.entity.Auditable;
import info.froylanvillaverde.shipments.config.dao.entity.Store;

import javax.persistence.*;

@Entity
@Table(name = "shipments", schema = "main")
public class Shipment extends Auditable {

    @Id
    @GeneratedValue(generator = "main.shipments_id_seq")
    @SequenceGenerator(
            name = "shipments_id_seq",
            sequenceName = "main.shipments_id_seq"
            //,initialValue = 1000
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "shipment_number")
    private Long shipmentNumber;
    @ManyToOne
    @JoinColumn(name = "origin_location", referencedColumnName = "id")
    private Store originLocation;
    @ManyToOne
    @JoinColumn(name = "destination_location", referencedColumnName = "id")
    private Store destinationLocation;
    @Column(name = "sender_name")
    private String senderPerson;
    @Column(name = "sender_phone")
    private String senderPersonPhone;
    @Column(name = "sender_address")
    private String senderAddress;
    @Column(name = "receiver_name")
    private String receiverPerson;
    @Column(name = "receiver_phone")
    private String receiverPersonPhone;
    @Column(name = "receiver_address")
    private String receiverAddress;
    @Column(name = "shipment_content")
    private String shipmentContent;
    @Column(name = "sub")
    private String sub;
    @Column(name = "weight")
    private String weight;
    @Column(name = "cost")
    private String cost;
    @Column(name = "v_estimated")
    private String vEstimated;
    @Column(name = "tax")
    private String tax;
    @Column(name = "delivered")
    private Boolean delivered;
    @Column(name = "comments")
    private String comments;
    @Column(name = "quantity")
    private Integer quantity;
    @ManyToOne
    @JoinColumn(name = "shipmentreport_id", referencedColumnName = "id")
    private ShipmentsReport shipmentsReport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(Long shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public Store getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(Store originLocation) {
        this.originLocation = originLocation;
    }

    public Store getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(Store destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getSenderPerson() {
        return senderPerson;
    }

    public void setSenderPerson(String senderPerson) {
        this.senderPerson = senderPerson;
    }

    public String getSenderPersonPhone() {
        return senderPersonPhone;
    }

    public void setSenderPersonPhone(String senderPersonPhone) {
        this.senderPersonPhone = senderPersonPhone;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getReceiverPerson() {
        return receiverPerson;
    }

    public void setReceiverPerson(String receiverPerson) {
        this.receiverPerson = receiverPerson;
    }

    public String getReceiverPersonPhone() {
        return receiverPersonPhone;
    }

    public void setReceiverPersonPhone(String receiverPersonPhone) {
        this.receiverPersonPhone = receiverPersonPhone;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getShipmentContent() {
        return shipmentContent;
    }

    public void setShipmentContent(String shipmentContent) {
        this.shipmentContent = shipmentContent;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getvEstimated() {
        return vEstimated;
    }

    public void setvEstimated(String vEstimated) {
        this.vEstimated = vEstimated;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Boolean getDelivered() {
        return delivered;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ShipmentsReport getShipmentsReport() {
        return shipmentsReport;
    }

    public void setShipmentsReport(ShipmentsReport shipmentsReport) {
        this.shipmentsReport = shipmentsReport;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "id=" + id +
                ", shipmentNumber=" + shipmentNumber +
                ", originLocation=" + originLocation +
                ", destinationLocation=" + destinationLocation +
                ", senderPerson='" + senderPerson + '\'' +
                ", senderPersonPhone='" + senderPersonPhone + '\'' +
                ", senderAddress='" + senderAddress + '\'' +
                ", receiverPerson='" + receiverPerson + '\'' +
                ", receiverPersonPhone='" + receiverPersonPhone + '\'' +
                ", receiverAddress='" + receiverAddress + '\'' +
                ", shipmentContent='" + shipmentContent + '\'' +
                ", sub='" + sub + '\'' +
                ", weight='" + weight + '\'' +
                ", cost='" + cost + '\'' +
                ", vEstimated='" + vEstimated + '\'' +
                ", tax='" + tax + '\'' +
                ", delivered=" + delivered +
                ", comments='" + comments + '\'' +
                ", quantity=" + quantity +
                ", shipmentsReport=" + shipmentsReport +
                '}';
    }
}
