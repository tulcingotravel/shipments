package info.froylanvillaverde.shipments.main.dao.entity;

import info.froylanvillaverde.shipments.config.dao.entity.Auditable;
import info.froylanvillaverde.shipments.config.dao.entity.Person;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "shipments_report", schema = "main")
public class ShipmentsReport {

    @Id
    @GeneratedValue(generator = "main.shipments_report_id_seq")
    @SequenceGenerator(
            name = "shipments_report_id_seq",
            sequenceName = "main.shipments_report_id_seq"
    )
    @Column(name = "id")
    private Long id;

    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    private Person createdBy;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Person getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Person createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "ShipmentsReport{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", createdBy=" + createdBy +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
