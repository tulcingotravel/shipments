package info.froylanvillaverde.shipments.main.model;

public class ShipmentDto {

    private Long id;
    private Long shipmentNumber;
    private Long originLocation;
    private Long destinationLocation;
    private String senderPerson;
    private String senderPersonPhone;
    private String senderAddress;
    private String receiverPerson;
    private String receiverPersonPhone;
    private String receiverAddress;
    private String shipmentContent;
    private String sub;
    private String weight;
    private String cost;
    private String vEstimated;
    private String tax;
    private Boolean delivered;
    private String comments;
    private Integer quantity;
    private Long shipmentReportId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(Long shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public Long getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(Long originLocation) {
        this.originLocation = originLocation;
    }

    public Long getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(Long destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getSenderPerson() {
        return senderPerson;
    }

    public void setSenderPerson(String senderPerson) {
        this.senderPerson = senderPerson;
    }

    public String getSenderPersonPhone() {
        return senderPersonPhone;
    }

    public void setSenderPersonPhone(String senderPersonPhone) {
        this.senderPersonPhone = senderPersonPhone;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getReceiverPerson() {
        return receiverPerson;
    }

    public void setReceiverPerson(String receiverPerson) {
        this.receiverPerson = receiverPerson;
    }

    public String getReceiverPersonPhone() {
        return receiverPersonPhone;
    }

    public void setReceiverPersonPhone(String receiverPersonPhone) {
        this.receiverPersonPhone = receiverPersonPhone;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getShipmentContent() {
        return shipmentContent;
    }

    public void setShipmentContent(String shipmentContent) {
        this.shipmentContent = shipmentContent;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getvEstimated() {
        return vEstimated;
    }

    public void setvEstimated(String vEstimated) {
        this.vEstimated = vEstimated;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Boolean getDelivered() {
        return delivered;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getShipmentReportId() {
        return shipmentReportId;
    }

    public void setShipmentReportId(Long shipmentReportId) {
        this.shipmentReportId = shipmentReportId;
    }

    @Override
    public String toString() {
        return "ShipmentDto{" +
                "id=" + id +
                ", shipmentNumber=" + shipmentNumber +
                ", originLocation=" + originLocation +
                ", destinationLocation=" + destinationLocation +
                ", senderPerson='" + senderPerson + '\'' +
                ", senderPersonPhone='" + senderPersonPhone + '\'' +
                ", senderAddress='" + senderAddress + '\'' +
                ", receiverPerson='" + receiverPerson + '\'' +
                ", receiverPersonPhone='" + receiverPersonPhone + '\'' +
                ", receiverAddress='" + receiverAddress + '\'' +
                ", shipmentContent='" + shipmentContent + '\'' +
                ", sub='" + sub + '\'' +
                ", weight='" + weight + '\'' +
                ", cost='" + cost + '\'' +
                ", vEstimated='" + vEstimated + '\'' +
                ", tax='" + tax + '\'' +
                ", delivered=" + delivered +
                ", comments='" + comments + '\'' +
                ", quantity=" + quantity +
                ", shipmentReportId=" + shipmentReportId +
                '}';
    }
}
