package info.froylanvillaverde.shipments.main.model.mapper;

import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.config.service.StoreService;
import info.froylanvillaverde.shipments.main.dao.entity.Shipment;
import info.froylanvillaverde.shipments.main.dao.entity.ShipmentsReport;
import info.froylanvillaverde.shipments.main.model.ShipmentDto;
import info.froylanvillaverde.shipments.main.service.ShipmentsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Component
public class ShipmentMapper {

    @Autowired
    private StoreService storeService;
    @Autowired
    private ShipmentsReportService shipmentsReportService;

    @Transactional
    public ShipmentDto convertToDto(Shipment shipment){
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setCost(shipment.getCost());
        shipmentDto.setShipmentNumber(shipment.getShipmentNumber());
        shipmentDto.setComments(shipment.getComments());
        shipmentDto.setShipmentContent(shipment.getShipmentContent());
        shipmentDto.setId(shipment.getId());//validaciones nulls
        shipmentDto.setDelivered(shipment.getDelivered());
        shipmentDto.setDestinationLocation(shipment.getDestinationLocation().getId());//validaciones null
        shipmentDto.setOriginLocation(shipment.getOriginLocation().getId());//validaciones null
        shipmentDto.setReceiverAddress(shipment.getReceiverAddress());
        shipmentDto.setReceiverPerson(shipment.getReceiverPerson());
        shipmentDto.setReceiverPersonPhone(shipment.getReceiverPersonPhone());
        shipmentDto.setSenderAddress(shipment.getSenderAddress());
        shipmentDto.setSenderPerson(shipment.getSenderPerson());
        shipmentDto.setSenderPersonPhone(shipment.getSenderPersonPhone());
        shipmentDto.setSub(shipment.getSub());
        shipmentDto.setTax(shipment.getTax());
        shipmentDto.setvEstimated(shipment.getvEstimated());
        shipmentDto.setWeight(shipment.getWeight());
        shipmentDto.setQuantity(shipment.getQuantity());
        shipmentDto.setShipmentReportId(ObjectUtils.isEmpty(shipment.getShipmentsReport()) ? null : shipment.getShipmentsReport().getId());
        return shipmentDto;
    }

    public Shipment convertToEntity(ShipmentDto shipmentDto){
        Shipment shipment = new Shipment();

        Store originLocation = storeService.getStore(shipmentDto.getOriginLocation());
        Store destinationLocation = storeService.getStore(shipmentDto.getDestinationLocation());
        ShipmentsReport shipmentsReport = ObjectUtils.isEmpty(shipmentDto.getShipmentReportId())? null: shipmentsReportService.getshipmentsReportsById(shipmentDto.getShipmentReportId());

        shipment.setCost(shipmentDto.getCost());
        shipment.setShipmentNumber(shipmentDto.getShipmentNumber());
        shipment.setComments(shipmentDto.getComments());
        shipment.setShipmentContent(shipmentDto.getShipmentContent());
        shipment.setId(shipmentDto.getId());//validaciones nulls
        shipment.setDelivered(shipmentDto.getDelivered());
        shipment.setDestinationLocation(destinationLocation);//validaciones null
        shipment.setOriginLocation(originLocation);//validaciones null
        shipment.setReceiverAddress(shipmentDto.getReceiverAddress());
        shipment.setReceiverPerson(shipmentDto.getReceiverPerson());
        shipment.setReceiverPersonPhone(shipmentDto.getReceiverPersonPhone());
        shipment.setSenderAddress(shipmentDto.getSenderAddress());
        shipment.setSenderPerson(shipmentDto.getSenderPerson());
        shipment.setSenderPersonPhone(shipmentDto.getSenderPersonPhone());
        shipment.setSub(shipmentDto.getSub());
        shipment.setTax(shipmentDto.getTax());
        shipment.setvEstimated(shipmentDto.getvEstimated());
        shipment.setWeight(shipmentDto.getWeight());
        shipment.setQuantity(shipmentDto.getQuantity());
        shipment.setShipmentsReport(shipmentsReport);

        return shipment;
    }
}
