package info.froylanvillaverde.shipments.auth.resource;

import java.io.Serializable;

public class JwtTokenResponse implements Serializable {

    private static final long serialVersionUID = 8317676219297719109L;

    private final String token;

    private String username;

    private String role;

    public JwtTokenResponse(String token) {
        this.token = token;
    }

    public JwtTokenResponse(String token, String username, String role) {
        this.token = token;
        this.username = username;
        this.role = role;
    }

    public String getToken() {
        return this.token;
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }
}