package info.froylanvillaverde.shipments.auth;

import info.froylanvillaverde.shipments.auth.resource.AuthenticationException;
import info.froylanvillaverde.shipments.config.model.PersonDto;
import info.froylanvillaverde.shipments.config.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

@Service
public class JwtInMemoryUserDetailsService implements UserDetailsService {

  private Set<JwtUserDetails> inMemoryUserList = new HashSet<>();
  @Autowired
  private PersonService personService;

  public List<JwtUserDetails> getUsers(){
    List<PersonDto> people = personService.findAllPersons();
    people.forEach(person ->
            inMemoryUserList.add(new JwtUserDetails(person.getId(), person.getMail(), person.getPassword(), person.getRole().toString(), person.getActive())));
    return new ArrayList<>(inMemoryUserList);
  }
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<JwtUserDetails> findFirst = getUsers().stream()
            .filter(user -> user.getUsername().equals(username))
            .findFirst();
    UserDetails userDetails;

    if (!findFirst.isPresent()) {
      throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
    }else {
      userDetails = findFirst.get();
    }

    if (!userDetails.isEnabled()) {
      throw new AuthenticationException(String.format("USER_DISABLED '%s'.", username));
    }

    return userDetails;
  }

}







