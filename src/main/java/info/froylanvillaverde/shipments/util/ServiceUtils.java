package info.froylanvillaverde.shipments.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class ServiceUtils {

    public static String encryptPassword(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
