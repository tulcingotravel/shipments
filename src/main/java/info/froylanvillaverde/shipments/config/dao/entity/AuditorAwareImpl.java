package info.froylanvillaverde.shipments.config.dao.entity;

import info.froylanvillaverde.shipments.auth.JwtUserDetails;
import org.slf4j.Logger;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

public class AuditorAwareImpl implements AuditorAware<Long> {

    private static final Logger LOGGER = getLogger(AuditorAwareImpl.class);

    @Override
    public Optional<Long> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails principal = (JwtUserDetails) authentication.getPrincipal();
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
        LOGGER.info(String.format("Current auditor is : %s", Objects.toString(principal)));
        return Optional.of((principal).getId());
    }
}
