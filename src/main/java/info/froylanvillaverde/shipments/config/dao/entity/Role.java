package info.froylanvillaverde.shipments.config.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "role", schema = "config")
public class Role {
    @Id
    @GeneratedValue(generator = "config.role_id_seq")
    @SequenceGenerator(
            name = "role_id_seq",
            sequenceName = "config.role_id_seq"
    )
    @Column(name = "id")
    private Long Id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.MERGE)
    @JsonIgnore
    private Set<Person> person = new HashSet<Person>();

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getPerson() {
        return person;
    }

    public void setPerson(Set<Person> person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Role{" +
                "Id='" + Id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
