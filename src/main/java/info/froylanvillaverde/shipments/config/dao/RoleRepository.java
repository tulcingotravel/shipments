package info.froylanvillaverde.shipments.config.dao;

import info.froylanvillaverde.shipments.config.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
