package info.froylanvillaverde.shipments.config.dao;

import info.froylanvillaverde.shipments.config.dao.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("select p from Person p join fetch p.role join fetch p.store")
    public List<Person> findPersons();

    @Query("select p from Person p join fetch p.role join fetch p.store where p.id=:personId")
    public Person findPersonById(Long personId);

    @Query("select p from Person p join fetch p.role join fetch p.store where p.mail=:mail")
    public Person findPersonByMail(String mail);
}
