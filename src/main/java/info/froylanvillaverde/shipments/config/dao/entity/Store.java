package info.froylanvillaverde.shipments.config.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "store", schema = "config")
public class Store {

    @Id
    @GeneratedValue(generator = "config.store_id_seq")
    @SequenceGenerator(
            name = "store_id_seq",
            sequenceName = "config.store_id_seq"
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "phone")
    private String phone;
    @Column(name = "country")
    private String country;
    @Column(name = "folio")
    private String folio;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "folio_counter")
    private Long folioCounter;
    @OneToMany(mappedBy = "store", cascade = CascadeType.MERGE)
    @JsonIgnore
    private Set<Person> person = new HashSet<Person>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Person> getPerson() {
        return person;
    }

    public void setPerson(Set<Person> person) {
        this.person = person;
    }

    public Long getFolioCounter() {
        return folioCounter;
    }

    public void setFolioCounter(Long folioCounter) {
        this.folioCounter = folioCounter;
    }

    @Override
    public String toString() {
        return "Store{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", country='" + country + '\'' +
                ", folio='" + folio + '\'' +
                ", active=" + active +
                ", folioCounter=" + folioCounter +
                '}';
    }
}
