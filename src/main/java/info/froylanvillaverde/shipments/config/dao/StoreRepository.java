package info.froylanvillaverde.shipments.config.dao;

import info.froylanvillaverde.shipments.config.dao.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
}
