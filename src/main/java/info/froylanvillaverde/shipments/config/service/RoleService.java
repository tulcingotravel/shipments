package info.froylanvillaverde.shipments.config.service;

import info.froylanvillaverde.shipments.config.dao.RoleRepository;
import info.froylanvillaverde.shipments.config.dao.entity.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private RoleRepository repository;

    public List<Role> findAllRoles(){
        LOG.info("findAllRoles()");
        return repository.findAll();
    }

    public Role getRole(Long id){
        LOG.info("getRole() with id [{}]", id);
        return repository.findById(id).get();
    }

    public void saveRole(Role role){
        LOG.info("Role saved {}", role.toString());
        repository.save(role);
    }

    public Role updateRole(Role role) {
        LOG.info("Role updated {}", role.toString());
        return repository.save(role);
    }

    public void deleteById(Long id){
        LOG.info("Role deleted {}", id);
        repository.deleteById(id);
    }
}
