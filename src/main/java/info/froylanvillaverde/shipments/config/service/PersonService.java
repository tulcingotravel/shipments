package info.froylanvillaverde.shipments.config.service;

import info.froylanvillaverde.shipments.config.dao.PersonRepository;
import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.model.PersonDto;
import info.froylanvillaverde.shipments.config.model.mapper.PersonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private PersonRepository repository;

    @Autowired
    private PersonMapper personMapper;

    public List<PersonDto> findAllPersons(){
        LOG.info("findAllPersons()");
        List<Person> people = repository.findPersons();
        List<PersonDto> personDtos = new ArrayList<>();
        people.stream().forEach(person -> {
            PersonDto personDto = new PersonDto();
            personDto = personMapper.convertToDto(person);
            personDtos.add(personDto);
        });
        return personDtos;
    }

    public PersonDto getPersonDto(Long id){
        LOG.info("getPersonDto() with id [{}]", id);

        PersonDto personDto;
        if(id == -1) personDto = new PersonDto();
        else personDto = personMapper.convertToDto(repository.findPersonById(id));

        return personDto;
    }

    public PersonDto getPersonDtoByMail(String mail){
        LOG.info("getPersonDto() with mail [{}]", mail);

        PersonDto personDto;
        personDto = personMapper.convertToDto(repository.findPersonByMail(mail));

        return personDto;
    }

    public Person getPerson(Long id){
        LOG.info("getPerson() with id [{}]", id);

        Person person;
        if(id == -1) person = new Person();
        else person = repository.findPersonById(id);

        return person;
    }

    public void savePerson(PersonDto person){
        Person personSaved = personMapper.convertToEntity(person);
        try {
            repository.save(personSaved);
            LOG.info("Person saved {}", person.toString());
        }catch (Exception exception){
            LOG.error("Person NOT saved {}", exception.getLocalizedMessage());
            exception.printStackTrace();
        }

    }

    public PersonDto updatePerson(PersonDto person) {
        Person personEntity;
        Person personUpdate;

        personUpdate = personMapper.convertToEntity(person);
        try {
            repository.save(personUpdate);
            LOG.info("Person updated {}", personUpdate);
        }catch (Exception exception){
            LOG.error("Person NOT updated. {}", exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return person;
    }

    public void deleteById(Long id){
        try {
            repository.deleteById(id);
        }catch (Exception exception){
            LOG.error("Person NOT deleted. {}", exception.getLocalizedMessage());
            exception.printStackTrace();
        }
    }
}
