package info.froylanvillaverde.shipments.config.service;

import info.froylanvillaverde.shipments.config.dao.StoreRepository;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    StoreRepository repository;
    public List<Store> findAllOriginLocation(){
        LOG.info("findAllOriginLocation()");
        return repository.findAll();
    }

    public Store getStore(Long id){
        LOG.info("getStore() with id [{}]", id);
        return repository.findById(id).get();
    }

    public void saveStore(Store store){
        LOG.info("Store saved {}", store.toString());
        repository.save(store);
    }

    public Store updateStore(Store store) {
        //store.setUpdatedAt(LocalDateTime.now());
        LOG.info("Store updated {}", store.toString());
        return repository.save(store);
    }

    public void deleteById(Long id){
        LOG.info("Store deleted {}", id);
        repository.deleteById(id);
    }

}
