package info.froylanvillaverde.shipments.config.controller;

import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.config.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/config/store")
@CrossOrigin(origins = "http://localhost:3000")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @GetMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List> findAllOriginLocation(){

        return ResponseEntity.ok(storeService.findAllOriginLocation());
    }

    @GetMapping("/{storeId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Store> getStore(@PathVariable Long storeId){
        Store storeReturned;

        if (storeId == -1) storeReturned = new Store();
        else storeReturned = storeService.getStore(storeId);

        return ResponseEntity.ok(storeReturned);
    }

    @PutMapping("/{storeId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Store> updateStore(@RequestBody Store store){
        Store storeUpdated = storeService.updateStore(store);
        return new ResponseEntity<Store>(storeUpdated, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity saveStore(@RequestBody Store store){
        storeService.saveStore(store);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(store.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{storeId}")
    public ResponseEntity<Void> deleteById(@PathVariable long storeId){
        storeService.deleteById(storeId);
        return ResponseEntity.noContent().build();
    }
}
