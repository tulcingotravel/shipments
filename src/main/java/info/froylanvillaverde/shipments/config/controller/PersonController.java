package info.froylanvillaverde.shipments.config.controller;

import info.froylanvillaverde.shipments.config.model.PersonDto;
import info.froylanvillaverde.shipments.config.service.PersonService;
import info.froylanvillaverde.shipments.config.service.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/config/person")
@CrossOrigin(origins = "http://localhost:3000")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private StoreService storeService;

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    @GetMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<PersonDto>> findAllPersons(){

        return ResponseEntity.ok(personService.findAllPersons());
    }

    @GetMapping("/{personId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<PersonDto> getPerson(@PathVariable Long personId){

        PersonDto personReturned;

        personReturned = personService.getPersonDto(personId);
        LOG.debug(personReturned.toString());

        return ResponseEntity.ok(personReturned);
    }

    @GetMapping("/username/{username}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Map> getPersonByMail(@PathVariable String username){

        PersonDto personReturned;
        Map<String, String> personProfile = new HashMap<>();
        personReturned = personService.getPersonDtoByMail(username);
        personProfile.put("name", personReturned.getName() + " " + personReturned.getFirstName());
        personProfile.put("country", storeService.getStore(personReturned.getStore()).getCountry());
        personProfile.put("store", storeService.getStore(personReturned.getStore()).getId().toString());

        return ResponseEntity.ok(personProfile);
    }

    @PutMapping("/{personId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<PersonDto> updatePerson(@RequestBody PersonDto person){
        PersonDto personUpdated = personService.updatePerson(person);
        return new ResponseEntity<PersonDto>(personUpdated, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity savePerson(@RequestBody PersonDto person){
        personService.savePerson(person);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(person.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{personId}")
    public ResponseEntity<Void> deleteById(@PathVariable long personId){
        personService.deleteById(personId);
        return ResponseEntity.noContent().build();
    }
}
