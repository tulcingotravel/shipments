package info.froylanvillaverde.shipments.config.model.mapper;

import info.froylanvillaverde.shipments.config.dao.entity.Person;
import info.froylanvillaverde.shipments.config.dao.entity.Role;
import info.froylanvillaverde.shipments.config.dao.entity.Store;
import info.froylanvillaverde.shipments.config.model.PersonDto;
import info.froylanvillaverde.shipments.config.service.PersonService;
import info.froylanvillaverde.shipments.config.service.RoleService;
import info.froylanvillaverde.shipments.config.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Component
public class PersonMapper {

    @Autowired
    private RoleService roleService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private PersonService personService;

    @Transactional
    public PersonDto convertToDto(Person person){
        PersonDto personDto = new PersonDto();

        personDto.setActive(person.getActive());
        personDto.setBirthdate(person.getBirthdate());
        personDto.setFirstName(person.getFirstName());
        personDto.setId(person.getId());
        personDto.setMail(person.getMail());
        personDto.setName(person.getName());
        personDto.setPassword(person.getPassword());
        personDto.setPhone(person.getPhone());
        personDto.setLastName(person.getLastName());
        personDto.setRole(person.getRole().getId());
        personDto.setStore(person.getStore().getId());
        return personDto;
    }

    public Person convertToEntity(PersonDto personDto){
        Person person;

        Role role = roleService.getRole(personDto.getRole());
        Store store = storeService.getStore(personDto.getStore());

        person = ObjectUtils.isEmpty(personDto.getId())? new Person(): personService.getPerson(personDto.getId());
        person.setActive(personDto.getActive());
        person.setBirthdate(personDto.getBirthdate());
        person.setFirstName(personDto.getFirstName());
        person.setId(personDto.getId());
        person.setMail(personDto.getMail());
        person.setName(personDto.getName());
        person.setPassword(personDto.getPassword());
        person.setPhone(personDto.getPhone());
        person.setLastName(personDto.getLastName());
        person.setRole(role);
        person.setStore(store);
        return person;
    }
}
