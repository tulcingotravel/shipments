package info.froylanvillaverde.shipments.config.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum DestinationLocation {
    NEW_YORK(1, "NEW_YORK"),
    TEXAS(2, "TEXAS"),
    LOS_ANGELES(3, "LOS_ANGELES"),
    CHICAGO(4, "CHICAGO");

    private final Integer key;
    private final String name;

    /**
     * A mapping between the integer code and its corresponding text to facilitate lookup by code.
     */
    private static Map<Integer, DestinationLocation> destinationLocationMap;

    DestinationLocation(Integer key, String name) {
        this.key = key;
        this.name = name;
    }

    public static Map<Integer, DestinationLocation> getDestinationLocation() {
        destinationLocationMap = new HashMap<>();
        Arrays.stream(values()).forEach(destinationLocation -> destinationLocationMap.put(destinationLocation.key, destinationLocation));
        return destinationLocationMap;
    }

    public Integer getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}
