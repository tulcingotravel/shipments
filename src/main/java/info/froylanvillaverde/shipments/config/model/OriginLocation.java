package info.froylanvillaverde.shipments.config.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum OriginLocation {
    ACATLAN(1, "ACATLAN"),
    ACAXTLAHUACAN(2, "ACAXTLAHUACAN"),
    ALCOZAUCA(3, "ALCOZAUCA"),
    ALPOYECA(4, "ALPOYECA"),
    ANIO_DE_JUAREZ(5, "ANIO_DE_JUAREZ"),
    ATENCINGO(6, "ATENCINGO"),
    ATLX_PMAYORAZGO(7, "ATLX_PMAYORAZGO"),
    AXOCHIAPAN(8, "AXOCHIAPAN"),
    CHIAUTLA(9, "CHIAUTLA"),
    CHILAPA(10, "CHILAPA"),
    CHILPANCINGO(11, "CHILPANCINGO"),
    CHL_PMAYORAZGO(12, "CHL_PMAYORAZGO"),
    CUALAC(13, "CUALAC"),
    CUALAC_MIREYA(14, "CUALAC_MIREYA"),
    CUAUTLA_CENTRO(15, "CUAUTLA_CENTRO"),
    DISTRITO_FEDERAL(16, "DISTRITO_FEDERAL"),
    HUAJUAPAN(17, "HUAJUAPAN"),
    HUAMUX_LT(18, "HUAMUX_LT"),
    HUAMUXTITLAN(19, "HUAMUXTITLAN"),
    HUEHUETLAN(20, "HUEHUETLAN"),
    I_DE_MATAMOROS(21, "I_DE_MATAMOROS"),
    IXCAMILPA(22, "IXCAMILPA"),
    IXCATEOPAN(23, "IXCATEOPAN"),
    IZUCAR_PANCHO(24, "IZUCAR_PANCHO"),
    JOJUTLA(25, "JOJUTLA"),
    MOMOXPAN(26, "MOMOXPAN"),
    OLINALA_ELENA(26, "OLINALA_ELENA"),
    OLINALA_VICTOR(27, "OLINALA_VICTOR"),
    PUE_MAYORAZGO(28, "PUE_MAYORAZGO"),
    PUEBLA_VW(29, "PUEBLA_VW"),
    SAN_PEDRO_OCOTLAN(30, "SAN_PEDRO_OCOTLAN"),
    SANTA_ANA_RAYON(31, "SANTA_ANA_RAYON"),
    SN_MIGUEL_TILAPA(32, "SN_MIGUEL_TILAPA"),
    TE(33, "TE"),
    TEPEACA(34, "TEPEACA"),
    TLALT_MARILU(35, "TLALT_MARILU"),
    TLALTEPEXI(36, "TLALTEPEXI"),
    TLAPA(37, "TLAPA"),
    TOLTECAMILA(38, "TOLTECAMILA"),
    TULCINGO(39, "TULCINGO"),
    XIHUITLIPA(40, "XIHUITLIPA"),
    XOCHI(41, "XOCHI"),
    YUPILTEPEC(42, "YUPILTEPEC"),
    ZAPOTITLAN(43, "ZAPOTITLAN");

    private final Integer key;
    private final String name;

    /**
     * A mapping between the integer code and its corresponding text to facilitate lookup by code.
     */
    private static Map<Integer, OriginLocation> originLocationMap;

    OriginLocation(Integer key, String name) {
        this.key = key;
        this.name = name;
    }

    public static Map<Integer, OriginLocation> getOriginLocation(){
        originLocationMap = new HashMap<>();
        Arrays.stream(values()).forEach( originLocation -> originLocationMap.put(originLocation.key, originLocation));
        return originLocationMap;
    }

    public Integer getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}